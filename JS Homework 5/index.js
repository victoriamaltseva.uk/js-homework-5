/* 
    Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и 
    дополните ее следующим функционалом:

    При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и 
    сохранить ее в поле birthday.
    Создать метод getAge() который будет возвращать сколько пользователю лет.
    Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, 
    соединенную с фамилией(в нижнем регистре) и годом рождения. 
    (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).

    Вывести в консоль результат работы функции createNewUser(), 
    а также функций getAge() и getPassword() созданного объекта.
*/

const firstName = prompt('Введите имя');
const lastName = prompt('Введите фамилию');

function createNewUser() {
    let userDateOfBirth = prompt('Введите дату рождения в формате dd.mm.yyyy');
    const newUser = {
        firstName,
        lastName,
        userDateOfBirth,
        getLogin() {
            return(this.firstName.charAt(0) + this.lastName).toLowerCase()
        },

        getPassword() {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.userDateOfBirth.split('.')[2];
        },
    
        getAge() {
            let date = new Date();
            const currentYear = date.getFullYear();
            const userAge = currentYear - this.userDateOfBirth.split('.')[2]
            return userAge
        }
    }

    return newUser
};

const user = createNewUser()
console.log(user);
console.log(user.getPassword());
console.log(user.getAge());

